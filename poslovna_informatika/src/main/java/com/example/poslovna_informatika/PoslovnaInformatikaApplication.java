package com.example.poslovna_informatika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoslovnaInformatikaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoslovnaInformatikaApplication.class, args);
	}

}
